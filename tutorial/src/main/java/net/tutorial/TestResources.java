package net.tutorial;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestResources {
    @RequestMapping(value = "/hello")
    public String getMsg(){
        return "Hello World";
    }
}
