const express = require('express')
const app = express()

app.get('/', (req, res) => {
    var my_json = {
        from : 'server',
        apiValues : process.env.NODE
    }
    res.send(my_json);
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));