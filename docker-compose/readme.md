# Load balancing example with node appllication with nginx 
```
docker network create node_app_net

docker run --name app_1 -d --net node_app_net -e NODE=1.1.1 node_application

docker run --name app_2 -d --net node_app_net -e NODE=2.2.2 node_application

docker run --name app_loadbalancer -p 8080:80 --net node_app_net -v /Users/samit/workspace/docker/docker-compose/loadbalancer/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx
```

# docker compose
> is a CLI tool
> available version are 1,2,2.1,3,3.1
> the available cli are :
```
docker-compose --help
docker-compose -f fileName.yml #If the file name is other then docker-compose then specify the file naname like below
docker-compose up #start the docker compose 
docker-compose down #stop and delete all container created by up
```
> The syntext looks like
```
version: '3.1' # if no version is specified then v1 is assumed, recommend v2 minimum
services: #container, same as docker run
   serviceName: # a friendly name also the DNS name inside container
     image: # image name of the container, Optional for build
     command: # Optional, replace the default CMD specified by image
     environment: # Optional, same as -e command in docker run
        -
     volumes: #Optional, same as -v with docker run
        -.:site
     ports : #Optional , same as -p with docker run
        -'80:4000'
     depends_on: # Optionals, if the container is dependent on other container
        - serviceName
   serviceName1: 
        -----
        -----
volumes: #optional, same as docker volume create
networks: #optional, same as docker network create
```
> The example can be found in the folder